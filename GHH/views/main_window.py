#################################################################
#################################################################

# Author: martinja
# Contact: javier.martinez.samblas@cern.ch

#################################################################
#################################################################

# IMPORTS

from GHH.__imports__ import *

#################################################################
#################################################################

class MyMainWindow(QMainWindow):

    #----------------------------------------------#

    def __init__(self, app, window_title = "", screen_resize_factor = 4/5, app_root_path = None):

        # inherit from QMainWindow
        QMainWindow.__init__(self)

        # main attributes
        self.app = app
        self.app_root_path = app_root_path
        self.window_title = window_title
        self.screen_resize_factor = screen_resize_factor

        # set the real path
        self.real_path = os.path.dirname(os.path.realpath(os.path.dirname(__file__)))
        if not self.app_root_path:
            self.app_root_path = self.real_path

        # build and bind widgets
        self.buildCodeWidgets()
        self.bindWidgets()

        # set title and icon
        self.setWindowTitle(self.window_title)

        # rescale and adjust to the screen
        self.windowAdjustmentAndResizing()

        # finally show the window
        self.show()

        return

    #----------------------------------------------#

    def windowAdjustmentAndResizing(self):

        # get the current screen size of the user
        screen_object = self.app.primaryScreen()
        screen_size = screen_object.size()
        screen_rect = screen_object.availableGeometry()

        # minimum size for the main window
        self.setMinimumSize(1200, 600)

        # resize main window
        self.resize(screen_rect.width() * self.screen_resize_factor, screen_rect.height() * self.screen_resize_factor)

        # center main window
        frame_geometry = self.frameGeometry()
        frame_geometry.moveCenter(QDesktopWidget().availableGeometry().center())
        self.move(frame_geometry.topLeft())

        return

    #----------------------------------------------#

    def buildCodeWidgets(self):

        # create central widget
        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)

        # main layout
        self.verticalLayout_main_form = QVBoxLayout(self.central_widget)
        self.verticalLayout_main_form.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_main_form.setSpacing(0)
        self.verticalLayout_main_form.setObjectName("verticalLayout_stack")

        # set main layout
        self.central_widget.setLayout(self.verticalLayout_main_form)

        ################################################################################################
        # FRAME 1
        ################################################################################################

        # create frame
        self.frame1 = QFrame(self)
        self.frame1.setFrameShape(QFrame.Panel)
        self.frame1.setFrameShadow(QFrame.Plain)
        self.frame1.setObjectName("frame1")

        # layout of the frame
        self.horizontalLayout_frame1 = QHBoxLayout(self.frame1)
        self.horizontalLayout_frame1.setObjectName("horizontalLayout_frame1")
        self.horizontalLayout_frame1.setContentsMargins(15, 15, 15, 15)

        # add frame to the main layout
        self.verticalLayout_main_form.addWidget(self.frame1)

        # spacer to move everything to the right
        self.spacerItem = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.horizontalLayout_frame1.addItem(self.spacerItem)

        # create the RBAC button
        self.rbac_button = RbaButton(parent = self.frame1)
        self.rbac_button.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.rbac_button.setFixedSize(40, 40)
        self.horizontalLayout_frame1.addWidget(self.rbac_button)

        ################################################################################################
        # FRAME 2
        ################################################################################################

        # create frame
        self.frame2 = QFrame(self)
        self.frame2.setFrameShape(QFrame.Panel)
        self.frame2.setFrameShadow(QFrame.Plain)
        self.frame2.setObjectName("frame2")

        # layout of the frame
        self.horizontalLayout_frame2 = QHBoxLayout(self.frame2)
        self.horizontalLayout_frame2.setObjectName("horizontalLayout_frame2")
        self.horizontalLayout_frame2.setContentsMargins(15, 15, 15, 15)

        # add frame to the main layout
        self.verticalLayout_main_form.addWidget(self.frame2)

        # create tab widget
        self.tabWidget = QTabWidget(self.frame2)
        self.tabWidget.setTabBarAutoHide(False)
        self.tabWidget.setObjectName("tabWidget")

        # tab1
        self.tab1 = QWidget(self.frame2)
        self.tab1.setObjectName("tab1")
        self.tabWidget.addTab(self.tab1, "Tab1")

        # tab2
        self.tab2 = QWidget(self.frame2)
        self.tab2.setObjectName("tab2")
        self.tabWidget.addTab(self.tab2, "Tab2")

        # tab3
        self.tab3 = QWidget(self.frame2)
        self.tab3.setObjectName("tab3")
        self.tabWidget.addTab(self.tab3, "Tab3")

        # add tabwidget to the holder layout
        self.horizontalLayout_frame2.addWidget(self.tabWidget)

        # click init tab
        self.tabWidget.setCurrentIndex(0)

        ################################################################################################
        # FRAME 3
        ################################################################################################

        # create frame
        self.frame3 = QFrame(self)
        self.frame3.setFrameShape(QFrame.Panel)
        self.frame3.setFrameShadow(QFrame.Plain)
        self.frame3.setObjectName("frame3")

        # layout of the frame
        self.horizontalLayout_frame3 = QHBoxLayout(self.frame3)
        self.horizontalLayout_frame3.setObjectName("horizontalLayout_frame3")
        self.horizontalLayout_frame3.setContentsMargins(15, 15, 15, 15)

        # add frame to the main layout
        self.verticalLayout_main_form.addWidget(self.frame3)

        # label that will be replaced by log
        self.label_log = QLabel(self.frame3)
        self.label_log.setText("This label will be replaced by the log console")
        self.label_log.setObjectName("label_log")
        self.label_log.setAlignment(Qt.AlignCenter)
        self.horizontalLayout_frame3.addWidget(self.label_log)

        ################################################################################################
        ################################################################################################

        # SET STRETCH
        self.verticalLayout_main_form.setStretch(0,10)
        self.verticalLayout_main_form.setStretch(1,80)
        self.verticalLayout_main_form.setStretch(2,10)

        return

    #----------------------------------------------#

    def bindWidgets(self):

        return

    #----------------------------------------------#

#################################################################
#################################################################