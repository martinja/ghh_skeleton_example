#################################################################
#################################################################

# Author: martinja
# Contact: javier.martinez.samblas@cern.ch

#################################################################
#################################################################

# IMPORTS

# pyqt imports
from PyQt5.QtWidgets import (QMainWindow, QApplication, QFrame, QDesktopWidget, QWidget, QToolBar, QAction, QMessageBox, QFileDialog, QDialog)
from PyQt5.QtWidgets import (QSplitter, QProgressDialog, QPushButton, QLabel, QSpacerItem, QToolButton, QComboBox, QCheckBox, QScrollArea, QAbstractScrollArea, QGroupBox, QLineEdit)
from PyQt5.QtWidgets import (QBoxLayout, QVBoxLayout, QHBoxLayout, QGridLayout, QSizePolicy, QStyledItemDelegate, QShortcut)
from PyQt5.QtWidgets import (QTableView, QAbstractItemView, QHeaderView, QTabWidget, QStackedWidget)
from PyQt5.QtGui import (QIcon, QStandardItemModel, QStandardItem, QBrush, QColor, QPixmap, QFont)
from PyQt5.QtCore import (QSize, Qt, QTimer, QEventLoop, QAbstractTableModel, QObject, pyqtSignal, QPoint, QSettings, QRect, QThread, QStringListModel, QMetaObject, Q_ARG)
from PyQt5.Qt import (QItemSelectionModel, QMenu)
from PyQt5 import (uic)

# cern imports
from accwidgets.qt import exec_app_interruptable
from accwidgets.rbac import RbaButton

# other imports
import os
import sys
import re

#################################################################
#################################################################