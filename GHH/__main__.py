#################################################################
#################################################################

# Author: martinja
# Contact: javier.martinez.samblas@cern.ch

#################################################################
#################################################################

# IMPORTS

from GHH.__imports__ import *
from GHH.views.main_window import MyMainWindow

#################################################################
#################################################################

if (__name__ == '__main__'):

    # init app
    print("Starting the application...")
    app = QApplication(sys.argv)

    # inputs

    window_title = "GHH APP"
    screen_resize_factor = 4/5
    app_root_path = None

    # init main window
    main_window = MyMainWindow(app, window_title = window_title, screen_resize_factor = screen_resize_factor, app_root_path = app_root_path)

    # enter into the application running loop
    sys.exit(exec_app_interruptable(app))

#################################################################
#################################################################